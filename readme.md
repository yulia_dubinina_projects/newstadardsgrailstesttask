#Тестовое задание для соискателей

Тестовое задание должно быть выполнено используя grails либо java 8 с spring framework (для доступа к БД можно
использовать jdbc, jdbc template, hibernate template, my batis, any JPA 2 implementation (open jpa, hibernate, top
link, etc.)).

Предеметная область – сервис для поиска музыкальных композиций, авторов, композиторов, исполнителей, альбомов,
синглов, сборников. Ниже приводится список обязательных требований, которые нужно учесть в модели данных. Список
атрибутов таблиц не приводится специально, соискателю нужно создать таблицы самостоятельно

##Требования которые нужно учесть.
* Музыкальная композиция может быть исполнена разными исполнителями. Абсолютно разными с разные периоды времени,
  либо дуэтом, квартетом и т.д
* Каждая музыкальная композиция должна иметь одного композитора и автора текста.
* При этом исполнитель, композитор, автор могут быть одним и тем же человеком
* Музыкальная композиция может выпускаться синглом, либо внутри альбома (список песен одного исполнителя или
  группы), либо в составе сборника (песни разных групп/исполнителей на одном диске)
* Исполнитель может быть либо группой, либо как отдельный человек.
* Исполнитель может менять группу, то есть например петь с 1990 по 1995 год в одной группе, а с 1996 по 2000 год в
  другой. Причем даты могут пересекаться, то есть он может петь одновременно в нескольких группах.
##Результат выполнения задания.
* Исходные коды, проект собираемый в war gradle.
* ER модель данных.
* Проект при запуске должен создавать в БД тестовые данные, минимум 30 композиций, 6 альбомов (из них два
  сборника), 10 исполнителей, 5 групп, 10 композиторов, 10 авторов
* Также данные должны содержать минимум двух исполнителей, которые пели в разных группах.
* Графический интерфейс для поиска по БД:
  * Найти всех исполнителей которые исполняли композицию (композиция – drop down view, то есть выбирается из
    списка)
  * Найти все группы в которых пел заданный исполнитель.
  * Найти все альбомы на которых издавалась заданная композиция.
  * Найти все композиции заданного композитора
  * Найти все композиции заданного автора
  * Найти все композиции заданного исполнителя.