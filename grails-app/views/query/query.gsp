<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${pageTitle}</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<h2>${pageTitle}</h2>
<%--<tiles:insertAttribute name="query-content"/>--%>


<form action="${request.forwardURI}" method="get">
    <select name="id">
        <g:each var="selectItem" in="${selectFromList}">
            <g:set var="val" value="${selectItem.id}"/>
            <g:if test="${selectedType}">
                <g:set var="val" value="${val}_${selectItem['class'].simpleName}"/>
            </g:if>
            <g:set var="sel" value=""/>
            <g:if test="${selectedId != null && selectedId == selectItem.id}">
                <g:if test="${selectedType}">
                    <g:if test="${selectedType == selectItem['class'].simpleName}">
                        <g:set var="sel" value="selected"/>
                    </g:if>
                </g:if>
                <g:else>
                    <g:set var="sel" value="selected"/>
                </g:else>
            </g:if>
            <option value="${val}" ${sel}>${selectItem.displayString}</option>
        </g:each>
    </select>
    <input type="submit" value="Go!"/>
</form>
<g:if test="${selectedId}">
    <g:if test="${resultList}">
        <g:each var="resultItem" in="${resultList}">
            ${resultItem.displayString}<br>
        </g:each>
    </g:if>
    <g:else>
        Nothing was found...
    </g:else>
</g:if>

</body>
</html>
