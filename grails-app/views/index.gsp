<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>New Standards Demo</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>

<div id="content" role="main">
    <h2>Тестовое задание для соискателей.</h2>

    <p>Тестовое задание должно быть выполнено используя grails либо java 8 с spring framework (для доступа к БД можно
    использовать jdbc, jdbc template, hibernate template, my batis, any JPA 2 implementation (open jpa, hibernate, top
    link, etc.)).</p>

    <p>Предеметная область – сервис для поиска музыкальных композиций, авторов, композиторов, исполнителей, альбомов,
    синглов, сборников. Ниже приводится список обязательных требований, которые нужно учесть в модели данных. Список
    атрибутов таблиц не приводится специально, соискателю нужно создать таблицы самостоятельно</p>

    <p>Требования которые нужно учесть.</p>
    <ol type="1">
        <li>Музыкальная композиция может быть исполнена разными исполнителями. Абсолютно разными с разные периоды времени,
        либо дуэтом, квартетом и т.д
        </li>
        <li>Каждая музыкальная композиция должна иметь одного композитора и автора текста.</li>
        <li>При этом исполнитель, композитор, автор могут быть одним и тем же человеком</li>
        <li>Музыкальная композиция может выпускаться синглом, либо внутри альбома (список песен одного исполнителя или
        группы), либо в составе сборника (песни разных групп/исполнителей на одном диске)
        </li>
        <li>Исполнитель может быть либо группой, либо как отдельный человек.</li>
        <li>Исполнитель может менять группу, то есть например петь с 1990 по 1995 год в одной группе, а с 1996 по 2000 год в
        другой. Причем даты могут пересекаться, то есть он может петь одновременно в нескольких группах.
        </li>
    </ol>

    <p>Результат выполнения задания.</p>
    <ol type="1">
        <li>Исходные коды, проект собираемый в war gradle.</li>
        <li>ER модель данных.</li>
        <li>Проект при запуске должен создавать в БД тестовые данные, минимум 30 композиций, 6 альбомов (из них два
        сборника), 10 исполнителей, 5 групп, 10 композиторов, 10 авторов
        </li>
        <li>Также данные должны содержать минимум двух исполнителей, которые пели в разных группах.</li>
        <li>Графический интерфейс для поиска по БД:
            <ol type="a">
                <li>Найти всех исполнителей которые исполняли композицию (композиция – drop down view, то есть выбирается из
                списка)
                </li>
                <li>Найти все группы в которых пел заданный исполнитель.</li>
                <li>Найти все альбомы на которых издавалась заданная композиция.</li>
                <li>Найти все композиции заданного композитора</li>
                <li>Найти все композиции заданного автора</li>
                <li>Найти все композиции заданного исполнителя.</li>
            </ol>
    </ol>
</div>

</body>
</html>
