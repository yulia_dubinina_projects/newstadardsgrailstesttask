package newstandardsdemograils

import java.time.LocalDate

class Performance {
    LocalDate date
    static belongsTo = [composition: Composition]
    boolean single;
    static hasMany = [performers: Performer]

    static constraints = {
        date(blank: false, nullable: false)
        composition(blank: false, nullable: false)
        single(blank: false, nullable: false)
        performers(blank: false, nullable: false)
    }
}
