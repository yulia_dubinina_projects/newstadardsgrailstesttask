package newstandardsdemograils

class Person extends Performer {
    String firstName
    String lastName

    static constraints = {
        firstName(blank: false, nullable: false)
        lastName(blank: false, nullable: false)
    }

    String getDisplayString() {
        return firstName + ' ' + lastName
    }

}
