package newstandardsdemograils

class Album extends Collection implements DisplayableAsString {
    static belongsTo = [performer: Performer]

    static constraints = {
        performer(blank: false, nullable: false)
    }

    String getDisplayString() {
        return 'Album "' + title + '" by ' + performer.getDisplayString()
    }

}
