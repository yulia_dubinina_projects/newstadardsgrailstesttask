package newstandardsdemograils

class Collection implements DisplayableAsString{
    static mapping = {
        tablePerHierarchy false
    }

    String title;
    static hasMany = [performances: Performance]

    static constraints = {
        title(blank: false, nullable: false)
        performances(blank: false, nullable: false)
    }
    String getDisplayString() {
        return 'Collection "' + title + '"'
    }
}
