package newstandardsdemograils

class Composition implements DisplayableAsString {
    String title
    static belongsTo = [
            author  : Person,
            composer: Person
    ]
    static constraints = {
        title(blank: false, nullable: false)
        author(blank: false, nullable: false)
        composer(blank: false, nullable: false)
    }

    String getDisplayString(){
        return '"' + title + '" (author: ' + author.getDisplayString() + '; composer: ' + composer.getDisplayString() + ')';
    }
}
