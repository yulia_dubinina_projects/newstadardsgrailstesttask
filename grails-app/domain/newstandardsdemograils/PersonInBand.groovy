package newstandardsdemograils

import java.time.LocalDate

class PersonInBand implements DisplayableAsString {

    LocalDate dateFrom;
    LocalDate dateTo;
    static belongsTo = [
            person: Person,
            band  : Band
    ]

    static constraints = {
        dateFrom(blank: false, nullable: false)
        dateTo(blank: false, nullable: true)
        person(blank: false, nullable: false)
        band(blank: false, nullable: false)
    }

    String getDisplayString() {
        if (dateTo == null)
            return person.getDisplayString() + ' plays in ' + band.getDisplayString() + ' since ' + dateFrom
        return person.getDisplayString() + ' played in ' + band.getDisplayString() + ' from ' + dateFrom + ' till ' + (dateTo == null ? 'present' : dateTo)
    }

}
