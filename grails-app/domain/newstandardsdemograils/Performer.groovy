package newstandardsdemograils

abstract class Performer implements DisplayableAsString {
    static mapping = {
        tablePerHierarchy false
        tablePerConcreteClass true
    }
    static constraints = {
    }
}
