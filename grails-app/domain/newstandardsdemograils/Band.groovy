package newstandardsdemograils

class Band extends Performer {

    String name;

    static constraints = {
        name(blank: false, nullable: false)

    }

    String getDisplayString() {
        return 'Band "' + name + '"'
    }

}
