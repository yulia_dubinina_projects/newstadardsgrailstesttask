package newstandardsdemograils

import grails.transaction.Transactional

import java.time.LocalDate

@Transactional
class BootstrapInitialDataService {

    def serviceMethod() {

    }


    class PerformancesWithSamePerformersInsertionHelper {
        List<Composition> compositions
        List<LocalDate> perfDates
        List<Boolean> isSingle
        List<Performer> performers

        void init4album(List<Composition> compositions, LocalDate defaultDate, Person person) {
            init(compositions, person)
            perfDates = [defaultDate] * compositions.size()
            isSingle = [false] * compositions.size()
        }

        void init4album(List<Composition> compositions, LocalDate defaultDate, Band band) {
            init(compositions, band)
            perfDates = [defaultDate] * compositions.size()
            isSingle = [false] * compositions.size()
        }

        void init(List<Composition> compositions) {
            this.compositions = compositions
        }

        void init(List<Composition> compositions, Person person) {
            init(compositions)
            performers = [person]
        }

        void init(List<Composition> compositions, Band band) {
            init(compositions)
            performers = [band]
        }

        void markAsSingle(int ind, LocalDate date) {
            perfDates[ind] = date
            isSingle[ind] = true
        }

        List<Performance> uploadPerformances() {
            List<Performance> result = []
            for (int i = 0; i < compositions.size(); i++) {
                Performance performance = new Performance(date: perfDates[i], single: isSingle[i],
                        composition: compositions[i], performers: performers)
                performance.save(failOnError: true)
                result.add(performance)
            }
            return result
        }

        Collection uploadAlbum(String title) {
            Album res = new Album(title: title, performer: performers[0])
            List<Performance> performances = uploadPerformances()
            for (Performance performance : performances) {
                res.addToPerformances(performance)
            }
            res.save(failOnError: true)
            return res
        }
    }

    void uploadPersons(Person... persons) {
        for (Person person : persons) {
            person.save(failOnError: true)
        }
    }

    Band uploadBand(Person person, LocalDate from, LocalDate to, String name) {
        Band band = new Band(name: name)
        band.save(failOnError: true)
        PersonInBand pib = new PersonInBand(person: person, band: band, dateFrom: from, dateTo: to)
        pib.save(failOnError: true)
        return band
    }

    Band uploadBand(List<Person> persons, List<LocalDate> from, List<LocalDate> to, String name) {
        Band band = new Band(name: name)
        band.save(failOnError: true)
        for (int i = 0; i < persons.size(); i++) {
            PersonInBand pib = new PersonInBand(person: persons[i], band: band, dateFrom: from[i], dateTo: to[i])
            pib.save(failOnError: true)
        }
        return band
    }

    private List<Composition> uploadCompositionsWithTheSameAuthorAndComposer(List<String> titles, Person author, Person composer) {
        List<Composition> compositions = []
        for (int i = 0; i < titles.size(); i++) {
            Composition composition = new Composition(title: titles[i], author: author, composer: composer)
            compositions.add(composition)
            composition.save(failOnError: true)
        }
        return compositions
    }

    private void uploadRelated2Trio() {
        //*******************Leps, Kobzon, Rozenbaum*******************
        Person leps = new Person(firstName: 'Григорий', lastName: 'Лепс');//performer
        Person kobzon = new Person(firstName: 'Иосиф', lastName: 'Кобзон');//composer,author,performer(kish)
        Person rosenbaum = new Person(firstName: 'Александр', lastName: 'Розенбаум');//composer,author,performer
        uploadPersons(leps, kobzon, rosenbaum)
        //Special case: single with three person at the same time
        List<Composition> compositions = uploadCompositionsWithTheSameAuthorAndComposer(['Вечерняя застольная'],
                rosenbaum, rosenbaum)
        Performance performance = new Performance(date: LocalDate.of(2011, 10, 31),
                composition: compositions[0], single: true, performers: [leps, rosenbaum, kobzon])
        performance.save(failOnError: true)
    }

    private void uploadRelated2Aria(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person kipelov = new Person(firstName: 'Валерий', lastName: 'Кипелов')
        Person dubinin = new Person(firstName: 'Виталий', lastName: 'Дубинин')
        Person pushkina = new Person(firstName: 'Маргарита', lastName: 'Пушкина')
        Person berkut = new Person(firstName: 'Артур', lastName: 'Беркут')
        Person mavrin = new Person(firstName: 'Сергей', lastName: 'Маврин')
        uploadPersons(kipelov, dubinin, pushkina, berkut, mavrin)
        //by Aria
        Band band = uploadBand([kipelov, dubinin, berkut, mavrin],
                [LocalDate.of(1985, 10, 31),
                 LocalDate.of(1987, 1, 1),
                 LocalDate.of(2002, 11, 9),
                 LocalDate.of(1987, 1, 1)],
                [LocalDate.of(2002, 8, 31),
                 null,
                 LocalDate.of(2011, 8, 31),
                 LocalDate.of(1995, 1, 31)],
                'Ария')
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(
                ['Паранойя',
                 'Уходи и не возвращайся',
                 'Дух войны'],
                pushkina, dubinin), LocalDate.of(1995, 9, 11), band)
        Collection album = perfInsHelper.uploadAlbum('Ночь короче дня')
        forCollection1.add(album.performances[2])
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(
                ['Крещение огнём',
                 'Твой новый мир',
                 'Там высоко',
                 'Бал у Князя Тьмы'],
                pushkina, dubinin), LocalDate.of(2003, 5, 29), band)
        perfInsHelper.uploadAlbum('Крещение огнём')
        //by Kipelov/Mavrin
        LocalDate date1 = LocalDate.of(1997, 1, 1)
        LocalDate date2 = LocalDate.of(1997, 12, 31)
        band = uploadBand([kipelov, mavrin], [date1, date1], [date2, date2], 'Кипелов/Маврин')
        List<Composition> c1 = uploadCompositionsWithTheSameAuthorAndComposer(
                ['Путь наверх',
                 'Смутное время',
                 'Ночь в июле'],
                pushkina, kipelov)
        //Special case: nightInJune - the same composition performed by different performers in different time
        //(see usages below)
        Composition nightInJune = c1[2]
        List<Composition> c2 = uploadCompositionsWithTheSameAuthorAndComposer(
                ['Свет дневной иссяк...',
                 'Castlevania'],
                pushkina, mavrin)
        perfInsHelper.init4album(c1 + c2, LocalDate.of(1997, 9, 10), band)
        perfInsHelper.uploadAlbum('Смутное время')
        //By Mavrin
        band = uploadBand([berkut, mavrin],
                [LocalDate.of(1998, 1, 1), LocalDate.of(1997, 1, 1)],
                [LocalDate.of(2000, 12, 31), null],
                'Маврин')
        c1 = uploadCompositionsWithTheSameAuthorAndComposer(
                ['Вольная птица',
                 'Город, стоящий у Солнца',
                 'Линия судьбы',
                 'Воины'],
                mavrin, mavrin)
        c2 = uploadCompositionsWithTheSameAuthorAndComposer(['Откровение'], pushkina, mavrin)
        perfInsHelper.init4album(c1 + c2, LocalDate.of(2006, 1, 1), band)
        perfInsHelper.uploadAlbum('Откровение')
        //By Kipelov
        date1 = LocalDate.of(2002, 1, 1)
        band = uploadBand([kipelov, mavrin],
                [date1, date1],
                [null, LocalDate.of(2004, 12, 31)],
                'Кипелов')
        perfInsHelper.init([nightInJune], band)
        perfInsHelper.markAsSingle(0, LocalDate.of(2009, 2, 19))
        perfInsHelper.uploadPerformances()
        perfInsHelper.init(uploadCompositionsWithTheSameAuthorAndComposer(['Мёртвая зона'],
                pushkina, kipelov))
        perfInsHelper.markAsSingle(0, LocalDate.of(2013, 4, 8))
        perfInsHelper.uploadPerformances()
    }

    private void uploadRelated2AgataKristy(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person vadimSamoilov = new Person(firstName: 'Вадим', lastName: 'Самойлов')
        Person glebSamoilov = new Person(firstName: 'Глеб', lastName: 'Самойлов')
        Person karasev = new Person(firstName: 'Михаил', lastName: 'Карасёв')
        uploadPersons(vadimSamoilov, glebSamoilov, karasev)
        //by Agata Kristy
        LocalDate date = LocalDate.of(1988, 1, 1)
        Band band = uploadBand([vadimSamoilov, glebSamoilov], [date, date], [null, null], 'Агата Кристи')
        List<Composition> c1 = uploadCompositionsWithTheSameAuthorAndComposer(
                ['Viva Kalman!',
                 'Пантера',
                 'Герои',
                 'Наша правда'],
                vadimSamoilov, vadimSamoilov)
        List<Composition> c2 = uploadCompositionsWithTheSameAuthorAndComposer(
                ['Сытая свинья',
                 'Праздник семьи',
                 'Бесса мэ…'],
                glebSamoilov, glebSamoilov)
        perfInsHelper.init4album(c1 + c2, LocalDate.of(1989, 1, 1), band)
        Collection album = perfInsHelper.uploadAlbum('Коварство и любовь')
        forCollection1.add(album.performances[1])
        //With Bi-2 (Special case: single with two bands at the same time)
        Band bi2 = new Band(name: 'Би-2')
        bi2.save(failOnError: true)
        c1 = uploadCompositionsWithTheSameAuthorAndComposer(['Всё, как он сказал'], karasev, karasev)
        Performance performance = new Performance(date: LocalDate.of(2007, 11, 16),
                composition: c1[0], single: true, performers: [band, bi2])
        performance.save(failOnError: true)
    }

    private void uploadRelated2SOAD(List<Performance> forCollection2, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person serj = new Person(firstName: 'Serj', lastName: 'Tankian')
        Person daron = new Person(firstName: 'Daron', lastName: 'Malakian')
        uploadPersons(serj, daron)
        //by Serj himself
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(
                ['Empty Walls',
                 'The Unthinking Majority',
                 'Money',
                 'Feed Us',
                 'Saving Us',
                 'Sky Is Over',
                 'Baby',
                 'Honking Antelope',
                 'Lie Lie Lie',
                 'Praise the Lord and Pass the Ammunition',
                 'Beethoven\'s Cunt',
                 'Elect the Dead'],
                serj, serj), LocalDate.of(2007, 10, 22), serj)
        perfInsHelper.markAsSingle(0, LocalDate.of(2007, 9, 10))
        perfInsHelper.markAsSingle(8, LocalDate.of(2007, 12, 24))
        perfInsHelper.markAsSingle(5, LocalDate.of(2008, 1, 16))
        Collection album = perfInsHelper.uploadAlbum('Elect the Dead')
        forCollection2.add(album.performances[9])
        forCollection2.add(album.performances[11])
        //by SOAD
        LocalDate date = LocalDate.of(1994, 1, 1)
        Band band = uploadBand([serj, daron], [date, date], [null, null], 'System of a Down')
        List<Composition> c1 = uploadCompositionsWithTheSameAuthorAndComposer(
                ['X',
                 'Forest',
                 'Science'],
                serj, daron)
        List<Composition> c2 = uploadCompositionsWithTheSameAuthorAndComposer(['Shimmy'], serj, serj)
        perfInsHelper.init4album(c1 + c2, LocalDate.of(2001, 9, 4), band)
        album = perfInsHelper.uploadAlbum('Toxicity')
        forCollection2.add(album.performances[1])
    }

    private void uploadRelated2Lidemann(List<Performance> forCollection2, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person lidemann = new Person(firstName: 'Till', lastName: 'Lindemann')
        Person peter = new Person(firstName: 'Peter', lastName: 'Tägtgren')
        uploadPersons(lidemann, peter)
        //by Ramstein
        uploadBand(lidemann, LocalDate.of(1994, 1, 1), null, 'Rammstein')
        //by Lindemann
        LocalDate date = LocalDate.of(2013, 1, 1)
        Band band = uploadBand([lidemann, peter], [date, date], [null, null], 'Lindemann')
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(
                ['Skills in Pills',
                 'Ladyboy',
                 'Fat',
                 'Fish On',
                 'Children of the Sun',
                 'Home Sweet Home',
                 'Cowboy',
                 'Golden Shower',
                 'Yukon',
                 'Praise Abort']
                , lidemann, peter), LocalDate.of(2015, 06, 22), band)
        perfInsHelper.markAsSingle(9, LocalDate.of(2015, 05, 28))
        perfInsHelper.markAsSingle(3, LocalDate.of(2015, 10, 9))
        Collection album = perfInsHelper.uploadAlbum('Skills in Pills')
        forCollection2.add(album.performances[3])
    }

    private void uploadRelated2Lagutenko(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person lagutenko = new Person(firstName: 'Илья', lastName: 'Лагутенко')
        uploadPersons(lagutenko)
        //by Муми Тролль
        Band band = uploadBand(lagutenko, LocalDate.of(1983, 1, 1), null, 'Муми Тролль')
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(
                ['Карнавала.Нет',
                 'Не Очень',
                 'Скорее И Быстро',
                 'Моя Певица',
                 'Северный Полюс',
                 'Невеста?',
                 'Жабры',
                 'Клубничная',
                 'Сны',
                 'Без Обмана',
                 'Ему Не Взять Тебя',
                 'Тише',
                 'Случайности']
                , lagutenko, lagutenko), LocalDate.of(2000, 2, 5), band)
        perfInsHelper.markAsSingle(5, LocalDate.of(1999, 11, 8))
        perfInsHelper.markAsSingle(0, LocalDate.of(1999, 12, 31))
        perfInsHelper.markAsSingle(9, LocalDate.of(2000, 5, 19))
        perfInsHelper.markAsSingle(3, LocalDate.of(2000, 12, 20))
        Collection album = perfInsHelper.uploadAlbum('Точно ртуть алоэ')
        forCollection1.add(album.performances[5])
    }

    private void uploadRelated2Zemfira(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person zemfira = new Person(firstName: 'Земфира', lastName: 'Рамазанова')
        Person brown = new Person(firstName: 'David Arthur', lastName: 'Brown')
        Person churikova = new Person(firstName: 'Инна', lastName: 'Чурикова')
        uploadPersons(zemfira, brown, churikova)
        //by Zemfira herself
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(
                ['В метро',
                 'Воскресенье',
                 'Дом',
                 'Мы разбиваемся',
                 'Мальчик',
                 'Господа',
                 'Я полюбила вас',
                 'Возьми меня',
                 'Снег начнётся',
                 '1000 лет',
                 'Во мне',
                 'Спасибо [репетиция, ноябрь 2006]'],
                zemfira, zemfira), LocalDate.of(2007, 10, 1), zemfira)
        perfInsHelper.markAsSingle(4, LocalDate.of(2007, 9, 1))
        perfInsHelper.markAsSingle(3, LocalDate.of(2007, 9, 4))
        perfInsHelper.markAsSingle(5, LocalDate.of(2008, 6, 9))
        perfInsHelper.markAsSingle(8, LocalDate.of(2008, 12, 1))
        Collection album = perfInsHelper.uploadAlbum('Спасибо')
        forCollection1.add(album.performances[3])
        //With Churikova (Special case: single with two person at the same time)
        Composition[] compositions = uploadCompositionsWithTheSameAuthorAndComposer(['Не пошлое'], zemfira, zemfira)
        Performance performance = new Performance(date: LocalDate.of(2004, 1, 1),
                composition: compositions[0], single: true, performers: [zemfira, churikova])
        performance.save(failOnError: true)
        //by The Uchpochmack
        Band band = uploadBand(zemfira, LocalDate.of(2013, 1, 1), null, 'The Uchpochmack')
        perfInsHelper.init(uploadCompositionsWithTheSameAuthorAndComposer(
                ['Someday',
                 'Lightbulbs'],
                zemfira, zemfira), band)
        perfInsHelper.markAsSingle(0, LocalDate.of(2013, 11, 5))
        perfInsHelper.markAsSingle(1, LocalDate.of(2013, 11, 19))
        perfInsHelper.uploadPerformances()
        //With Brown (Special case: single with band and person at the same time)
        compositions = uploadCompositionsWithTheSameAuthorAndComposer(['Mistress'], zemfira, zemfira)
        performance = new Performance(date: LocalDate.of(2013, 11, 12),
                composition: compositions[0], single: true, performers: [brown, band])
        performance.save(failOnError: true)
    }


    def initData() {
        long time = System.currentTimeMillis()
        long total = time
        List<Performance> forCollection1 = []
        List<Performance> forCollection2 = []
        PerformancesWithSamePerformersInsertionHelper perfInsHelper = new PerformancesWithSamePerformersInsertionHelper()

        time = System.currentTimeMillis()
        uploadRelated2Zemfira(forCollection1, perfInsHelper)
        sayTime('Uploading objects related to Zemfira', time)

        time = System.currentTimeMillis()
        uploadRelated2Lagutenko(forCollection1, perfInsHelper)
        sayTime('Uploading objects related to Lagutenko', time)

        time = System.currentTimeMillis()
        uploadRelated2Lidemann(forCollection2, perfInsHelper)
        sayTime('Uploading objects related to Lidemann', time)

        time = System.currentTimeMillis()
        uploadRelated2SOAD(forCollection2, perfInsHelper)
        sayTime('Uploading objects related to SOAD', time)

        time = System.currentTimeMillis()
        uploadRelated2AgataKristy(forCollection1, perfInsHelper)
        sayTime('Uploading objects related to AgataKristy', time)

        time = System.currentTimeMillis()
        uploadRelated2Aria(forCollection1, perfInsHelper)
        sayTime('Uploading objects related to Aria', time)

        time = System.currentTimeMillis()
        uploadRelated2Trio()
        sayTime('Uploading objects related to Trio', time)

        time = System.currentTimeMillis()
        Collection collection = new Collection(title: 'Популярное русское', performances: forCollection1)
        collection.save(failOnError: true)
        collection = new Collection(title: 'Популярное иностранное', performances: forCollection2)
        collection.save(failOnError: true)
        sayTime('Uploading collections', time)

        sayTime('TOTAL INIT DB', total)

    }

    void sayTime(String msg, long time) {
        println(msg + ' ' + (System.currentTimeMillis() - time) + 'ms')
    }

}
