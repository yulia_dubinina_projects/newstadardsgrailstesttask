package newstandardsdemograils

import java.time.LocalDate

class QueryController {

    def first() {
        String idStr = params.id
        long id
        if (idStr != null) {
            id = idStr as Long
        } else {
            id = -1
        }
        def modelMap = [pageTitle: 'Performers by composition', selectFromList: Composition.list()]
        if (id > 0) {
            modelMap['selectedId'] = id
            Set resSet = []
            def perfs = Performance.findAllByComposition(Composition.get(id))
            for (Performance perf : perfs) {
                resSet.addAll(perf.performers)
            }
            modelMap['resultList'] = resSet
        }
        render(view: 'query.gsp', model: modelMap)
    }

    def second() {
        String idStr = params.id
        long id
        if (idStr != null) {
            id = idStr as Long
        } else {
            id = -1
        }
        def modelMap = [pageTitle: 'Bands by performer', selectFromList: PersonInBand.list()*.person.toSet()]
        if (id > 0) {
            modelMap['selectedId'] = id
            modelMap['resultList'] = PersonInBand.findAllByPerson(Person.get(id))
        }
        render(view: 'query.gsp', model: modelMap)
    }

    def third() {
        String idStr = params.id
        long id
        if (idStr != null) {
            id = idStr as Long
        } else {
            id = -1
        }
        def modelMap = [pageTitle: 'Albums by composition', selectFromList: Composition.list()]
        if (id > 0) {
            modelMap['selectedId'] = id
            def perfs = Performance.findAllByComposition(Composition.get(id))
            def res
            if (!perfs.isEmpty()) {
                res = Collection.createCriteria().list {
                    performances {
                        'in'('id', perfs*.id)
                    }
                }
            } else {
                res = []
            }
            modelMap['resultList'] = res
        }
        render(view: 'query.gsp', model: modelMap)
    }

    def forth() {
        String idStr = params.id
        long id
        if (idStr != null) {
            id = idStr as Long
        } else {
            id = -1
        }
        def modelMap = [pageTitle: 'Compositions by composer', selectFromList: Composition.list()*.composer.toSet()]
        if (id > 0) {
            modelMap['selectedId'] = id
            modelMap['resultList'] = Composition.findAllByComposer(Person.get(id))
        }
        render(view: 'query.gsp', model: modelMap)
    }

    def fifth() {
        String idStr = params.id
        long id
        if (idStr != null) {
            id = idStr as Long
        } else {
            id = -1
        }
        def modelMap = [pageTitle: 'Compositions by author', selectFromList: Composition.list()*.author.toSet()]
        if (id > 0) {
            modelMap['selectedId'] = id
            modelMap['resultList'] = Composition.findAllByAuthor(Person.get(id))
        }
        render(view: 'query.gsp', model: modelMap)
    }

    def sixth() {
        String idStr = params.id
        long id
        if (idStr != null) {
            id = idStr as Long
        } else {
            id = -1
        }
        def modelMap = [pageTitle: 'Compositions by performer', selectFromList: Performer.list()]
        if (id > 0) {
            modelMap['selectedId'] = id
            Set res;
            def easyCase = Performance.createCriteria().list {
                performers {
                    eq('id', id)
                }
            }
            res = easyCase*.composition.toSet()

            def performer = Performer.get(id)
            if (performer instanceof Person) {
                def pibs = PersonInBand.findAllByPerson(performer)
                if (!pibs.isEmpty()) {
                    List<Performance> hardCase = Performance.createCriteria().list {
                        performers {
                            'in'('id', pibs*.band.id)
                        }
                    }
                    if (!hardCase.isEmpty()) {
                        Map rangesByBand = [:]
                        for (PersonInBand pib : pibs) {
                            List t = rangesByBand[pib.band]
                            if (t == null) {
                                t = []
                                rangesByBand[pib.band] = t
                            }
                            t.add([pib.dateFrom, pib.dateTo])
                        }
                        hardCase.removeAll {
                            boolean ok = false
                            for (Performer p : it.performers) {
                                List ranges = rangesByBand.get(p)
                                if (ranges != null) {
                                    for (List dates : ranges) {
                                        LocalDate d1 = dates[0]
                                        LocalDate d2 = dates[1]
                                        if (d1 <= it.date && (d2 == null || d2 >= it.date)) {
                                            ok = true
                                            break
                                        }

                                    }
                                }
                            }
                            return !ok
                        }
                        res.addAll(hardCase*.composition)
                    }
                }
            }

            modelMap['resultList'] = res
        }
        render(view: 'query.gsp', model: modelMap)
    }

}
