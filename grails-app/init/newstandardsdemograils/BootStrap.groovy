package newstandardsdemograils

class BootStrap {

    def initialDataService

    def init = { servletContext ->
        initialDataService.initData()
    }
    def destroy = {
    }
}
