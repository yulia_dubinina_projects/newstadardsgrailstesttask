package newstandardsdemograils

/**
 * Created by dhdaan on 11.07.2017.
 */
interface DisplayableAsString {

    String getDisplayString()
}